package com.cbsinc.cms.controllers;

/**
 * <p>
 * Title: Content Manager System
 * </p>
 * <p>
 * Description: System building web application develop by Konstantin Grabko.
 * Konstantin Grabko is Owner and author this code.
 * You can not use it and you cannot change it without written permission from Konstantin Grabko
 * Email: konstantin.grabko@yahoo.com or konstantin.grabko@gmail.com
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002-2014
 * </p>
 * <p>
 * Company: CENTER BUSINESS SOLUTIONS INC 
 * </p>
 * 
 * @author Konstantin Grabko
 * @version 1.0
 */

import java.text.SimpleDateFormat;

import com.cbsinc.cms.AuthorizationPageBean;
import com.cbsinc.cms.NotificationsBean;
import com.cbsinc.cms.annotations.PageController;
import com.cbsinc.cms.annotations.PostActionRequestMapping;
import com.cbsinc.cms.faceds.NotificationsFaced;
import com.cbsinc.cms.faceds.ProductPostAllFaced;

import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@PageController(jspName = "Notifications.jsp")
public class NotificationsAction implements IAction {

	NotificationsFaced notificationsFaced;
	SimpleDateFormat formatter;

	public boolean isInternet = true;

	public NotificationsAction() {
	}

	
	@PostActionRequestMapping(action = "unsubscribe")
	public void subscribeAction(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext)
			throws Exception {
		HttpSession session = request.getSession();
		AuthorizationPageBean authorizationPageBeanId = (AuthorizationPageBean) session.getAttribute("authorizationPageBeanId");
		String position_id = request.getParameter("product_id");
		ProductPostAllFaced productPostAllFaced = ServiceLocator.getInstance().getProductPostAllFaced();
		productPostAllFaced.unsubscribe(position_id,authorizationPageBeanId);
		authorizationPageBeanId.setStrMessage("The product was subscribed successfully");
		response.sendRedirect("ProductInfo.jsp?policy_byproductid=" + position_id);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext)
			throws Exception {
		doGet(request, response, servletContext);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext)
			throws Exception {

		AuthorizationPageBean authorizationPageBeanId;
		HttpSession session;
		NotificationsBean notificationsBean = null;
		notificationsFaced = ServiceLocator.getInstance().getNotificationsFaced();
		session = request.getSession();
		notificationsBean = (NotificationsBean) session.getAttribute("notificationsBeanId");
		authorizationPageBeanId = (AuthorizationPageBean) session.getAttribute("authorizationPageBeanId");
		if (authorizationPageBeanId == null || notificationsBean == null || notificationsFaced == null)
			return;

		request.setCharacterEncoding("UTF-8");
		if (request.getParameter("offset") != null && isNumber(request.getParameter("offset"))) {
			notificationsBean.setOffset(Integer.parseInt(request.getParameter("offset")));
		}
		if (request.getParameter("searchquery") != null && isNumber(request.getParameter("searchquery"))) {
			if (!notificationsBean.getSearchquery().equals(request.getParameter("searchquery")))
				notificationsBean.setOffset(0);
			notificationsBean.setSearchquery(request.getParameter("searchquery"));
		}

		if (notificationsBean.getSearchquery().equals("2")) {

			if (request.getParameter("datefrom") != null) {
				if (isNumber(request.getParameter("datefrom")))
					notificationsBean.setDateFrom(Long.parseLong(request.getParameter("datefrom")));
				else if (isDatePattern(request.getParameter("date_format")))
					notificationsBean.setDateFrom(request.getParameter("datefrom"), request.getParameter("date_format"),
							request.getLocale());
			}
			if (request.getParameter("dateto") != null) {
				if (isNumber(request.getParameter("dateto")))
					notificationsBean.setDateTo(Long.parseLong(request.getParameter("dateto")));
				else if (isDatePattern(request.getParameter("date_format")))
					notificationsBean.setDateTo(request.getParameter("dateto"), request.getParameter("date_format"),
							request.getLocale());
			}

			notificationsBean.setSelectOrderlistXML(notificationsFaced.getNotificationByDate(authorizationPageBeanId.getIntUserID(),
					notificationsBean, request.getLocale(), authorizationPageBeanId.getIntLevelUp(),
					authorizationPageBeanId.getSite_id()));
			
			notificationsBean.setSearchquery("0");
			return;
		}

		if (notificationsBean.getSearchquery().equals("3")) {

			if (request.getParameter("order_paystatus") != null) {
				notificationsBean.setOrder_paystatus_id(request.getParameter("order_paystatus"));
			}
			if (request.getParameter("order_status") != null) {
				notificationsBean.setDeliverystatus_id(request.getParameter("order_status"));
			}

			notificationsBean.setSelectOrderlistXML(notificationsFaced.getNotificationByStatus(authorizationPageBeanId.getSite_id(),
					notificationsBean, request.getLocale()));
			notificationsBean.setSelect_paystatus(notificationsFaced.getXMLDBList("Notifications.jsp?order_paystatus", "paystatus",
					notificationsBean.getOrder_paystatus_id(),
					"select  paystatus_id , lable  from  paystatus  where  active  = true"));
			notificationsBean.setSelect_deliverystatus(notificationsFaced.getXMLDBList("Notifications.jsp?order_deliverystatus",
					"deliverystatus", notificationsBean.getDeliverystatus_id(),
					"select  deliverystatus_id , lable  from  deliverystatus  where  active  = true and lang = '"
							+ authorizationPageBeanId.getLocale() + "' "));
			notificationsBean.setSearchquery("0");
			return;
		}

		notificationsBean.setSelectOrderlistXML(notificationsFaced.getNotificatios( notificationsBean, authorizationPageBeanId, request.getLocale()));
		notificationsBean.setSelect_paystatus(notificationsFaced.getXMLDBList("Notifications.jsp?order_paystatus", "paystatus",notificationsBean.getOrder_paystatus_id(),
				"select  paystatus_id , lable  from  paystatus  where  active  = true"));
		notificationsBean.setSelect_deliverystatus(notificationsFaced.getXMLDBList("Notifications.jsp?order_deliverystatus",
				"deliverystatus", notificationsBean.getDeliverystatus_id(),
				"select  deliverystatus_id , lable  from  deliverystatus  where  active  = true and lang = '"
						+ authorizationPageBeanId.getLocale() + "' "));

		notificationsBean.setSelect_menu_catalog(notificationsFaced.getMenuXMLDBList("Productlist.jsp?catalog_id", "menu",
				authorizationPageBeanId.getCatalog_id(),
				"select catalog_id , lable , parent_id  from catalog   where  active = true and parent_id = -2 and site_id = "
						+ authorizationPageBeanId.getSite_id() + " and lang_id = "
						+ authorizationPageBeanId.getLang_id()
						+ " or parent_id in (select catalog_id   from catalog   where  active = true and site_id = "
						+ authorizationPageBeanId.getSite_id() + "  and parent_id = -2 )"));

	}

	public boolean isNumber(String tmp) {
		if (tmp == null || tmp.length() == 0)
			return false;
		String IntField = "0123456789";
		for (int i = 0; i < tmp.length(); i++) {

			if (IntField.indexOf(tmp.charAt(i)) == -1) {
				if (tmp.charAt(i) != '-' && i != 0)
					return false;
			}
		}
		return true;
	}

	public boolean isDatePattern(String tmp) {
		if (tmp == null || tmp.length() == 0)
			return false;
		String IntField = "dm/yMDY:.";
		for (int i = 0; i < tmp.length(); i++) {

			if (IntField.indexOf(tmp.charAt(i)) == -1) {
				if (tmp.charAt(i) != '-' && i != 0)
					return false;
			}
		}
		return true;
	}
}
