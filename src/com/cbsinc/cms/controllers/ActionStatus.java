package com.cbsinc.cms.controllers;

public interface ActionStatus {
	
	final static int PRODUCT_ACTION_BID_NOTSUBMITED = -1;
	final static int PRODUCT_ACTION_BID_SUBMITED = 0;
	final static int PRODUCT_ACTION_BID_CANCELED = 1;
	final static int PRODUCT_ACTION_BID_WON = 2;
	final static int PRODUCT_ACTION_BID_DID_NOT_WIN = 3;

}
