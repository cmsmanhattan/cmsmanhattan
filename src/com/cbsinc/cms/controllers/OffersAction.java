package com.cbsinc.cms.controllers;

/**
 * <p>
 * Title: Content Manager System
 * </p>
 * <p>
 * Description: System building web application develop by Konstantin Grabko.
 * Konstantin Grabko is Owner and author this code.
 * You can not use it and you cannot change it without written permission from Konstantin Grabko
 * Email: konstantin.grabko@yahoo.com or konstantin.grabko@gmail.com
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002-2014
 * </p>
 * <p>
 * Company: CENTER BUSINESS SOLUTIONS INC 
 * </p>
 * 
 * @author Konstantin Grabko
 * @version 1.0
 */

import java.text.SimpleDateFormat;

import com.cbsinc.cms.AuthorizationPageBean;
import com.cbsinc.cms.OffersBean;
import com.cbsinc.cms.ProductlistBean;
import com.cbsinc.cms.annotations.PageController;
import com.cbsinc.cms.faceds.ProductlistFaced;

import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@PageController(jspName = "Offers.jsp")
public class OffersAction implements IAction {

	ProductlistFaced productlistFaced ;
	SimpleDateFormat formatter;

	public boolean isInternet = true;

	public OffersAction() {
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext)
			throws Exception {
		doGet(request, response, servletContext);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext)
			throws Exception {

		AuthorizationPageBean authorizationPageBeanId;
		HttpSession session;
		OffersBean offersBeanId = null;
		ProductlistBean productlistBeanId = null ;
		productlistFaced = ServiceLocator.getInstance().getProductlistFaced() ;
		session = request.getSession();
		offersBeanId = (OffersBean) request.getAttribute("offersBeanId");
		productlistBeanId = (ProductlistBean) request.getAttribute("productlistBeanId");
		
		productlistBeanId = new ProductlistBean();
		request.setAttribute("productlistBeanId", productlistBeanId);
		offersBeanId = new OffersBean();
		request.setAttribute("offersBeanId", offersBeanId);
		
		authorizationPageBeanId = (AuthorizationPageBean) session.getAttribute("authorizationPageBeanId");
		if (authorizationPageBeanId == null || offersBeanId == null || productlistFaced == null)
			return;

		request.setCharacterEncoding("UTF-8");
		if (request.getParameter("offset") != null && isNumber(request.getParameter("offset"))) {
			offersBeanId.setOffset(Integer.parseInt(request.getParameter("offset")));
		}
		if (request.getParameter("searchquery") != null && isNumber(request.getParameter("searchquery"))) {
			if (!offersBeanId.getSearchquery().equals(request.getParameter("searchquery")))
				offersBeanId.setOffset(0);
			offersBeanId.setSearchquery(request.getParameter("searchquery"));
		}

		if(productlistFaced.isSeller(authorizationPageBeanId)) {
		  productlistBeanId.offerResultSet = productlistFaced.getOffersReceiver(authorizationPageBeanId);
		}
		else 
		{
		  productlistBeanId.offerResultSet = productlistFaced.getOffersSubmitter(authorizationPageBeanId);
		}
		 
		offersBeanId.setSelectOffersXML(productlistBeanId.getOffers("" + authorizationPageBeanId.getIntUserID(),authorizationPageBeanId.getSite_id()));
		
		offersBeanId.setSelect_menu_catalog(productlistFaced.getMenuXMLDBList("Productlist.jsp?catalog_id", "menu",
				authorizationPageBeanId.getCatalog_id(),
				"select catalog_id , lable , parent_id  from catalog   where  active = true and parent_id = -2 and site_id = "
						+ authorizationPageBeanId.getSite_id() + " and lang_id = "
						+ authorizationPageBeanId.getLang_id()
						+ " or parent_id in (select catalog_id   from catalog   where  active = true and site_id = "
						+ authorizationPageBeanId.getSite_id() + "  and parent_id = -2 )"));

	}

	public boolean isNumber(String tmp) {
		if (tmp == null || tmp.length() == 0)
			return false;
		String IntField = "0123456789";
		for (int i = 0; i < tmp.length(); i++) {

			if (IntField.indexOf(tmp.charAt(i)) == -1) {
				if (tmp.charAt(i) != '-' && i != 0)
					return false;
			}
		}
		return true;
	}

	public boolean isDatePattern(String tmp) {
		if (tmp == null || tmp.length() == 0)
			return false;
		String IntField = "dm/yMDY:.";
		for (int i = 0; i < tmp.length(); i++) {

			if (IntField.indexOf(tmp.charAt(i)) == -1) {
				if (tmp.charAt(i) != '-' && i != 0)
					return false;
			}
		}
		return true;
	}
}
