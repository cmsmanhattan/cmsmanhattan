package com.cbsinc.cms.controllers;

import org.apache.log4j.Logger;

import com.cbsinc.cms.AuthorizationPageBean;
import com.cbsinc.cms.CatalogListBean;
import com.cbsinc.cms.ItemDescriptionBean;
import com.cbsinc.cms.annotations.PageController;
import com.cbsinc.cms.faceds.ProductInfoFaced;
import com.cbsinc.cms.faceds.ProductlistFaced;

/**
 * <p>
 * Title: Content Manager System
 * </p>
 * <p>
 * Description: System building web application develop by Konstantin Grabko.
 * Konstantin Grabko is Owner and author this code.
 * You can not use it and you cannot change it without written permission from Konstantin Grabko
 * Email: konstantin.grabko@yahoo.com or konstantin.grabko@gmail.com
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002-2014
 * </p>
 * <p>
 * Company: CENTER BUSINESS SOLUTIONS INC 
 * </p>
 * 
 * @author Konstantin Grabko
 * @version 1.0
 */

import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@PageController(jspName = "ProductInfoMarketPlace.jsp")
public class ProductInfoMarketPlaceAction implements IAction {

	transient static private Logger log = Logger.getLogger(PolicyAction.class);
	private ProductInfoFaced policyFaced = null;
	private ProductlistFaced productlistFaced = null;

	public void doPost(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext)
			throws Exception {
		doGet(request, response, servletContext);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext)
			throws Exception {

		AuthorizationPageBean authorizationPageBeanId;
		ItemDescriptionBean itemDescriptionBeanId = null;
		HttpSession session;
		boolean isInternet = true;

		if (policyFaced == null)
			policyFaced = ServiceLocator.getInstance().getPolicyFaced();
		if (productlistFaced == null)
			productlistFaced = ServiceLocator.getInstance().getProductlistFaced();
		itemDescriptionBeanId = new ItemDescriptionBean();
		session = request.getSession();

		authorizationPageBeanId = (AuthorizationPageBean) session.getAttribute("authorizationPageBeanId");
		request.setAttribute("itemDescriptionBeanId", itemDescriptionBeanId);

		if (request.getRemoteAddr().startsWith("192."))
			isInternet = false;
		if (request.getRemoteAddr().startsWith("10."))
			isInternet = false;

		if (authorizationPageBeanId == null || itemDescriptionBeanId == null)
			return;

		if (request.getParameter("policy_byproductid") == null && request.getParameter("page") == null) {

			if (request.getParameter("rate") != null && isNumber(request.getParameter("rate"))) {
				itemDescriptionBeanId.setProduct_id(Long.toString(authorizationPageBeanId.getLastProductId()));
				int rate = Integer.parseInt(request.getParameter("rate"));
				policyFaced.setRatring1(rate, itemDescriptionBeanId.getProduct_id());
				response.sendRedirect("ProductInfo.jsp?policy_byproductid=" + itemDescriptionBeanId.getProduct_id());
				return;
			}

			response.sendRedirect("ProductlistMarketPlace.jsp?catalog_id=-2");
			return;
		}

		request.setCharacterEncoding("UTF-8");

		itemDescriptionBeanId.internet = isInternet;

		if (request.getParameter("policy_byproductid") != null
				&& isNumber(request.getParameter("policy_byproductid"))) {
			policyFaced.setProductInfoBean(authorizationPageBeanId.getIntUserID(),
					request.getParameter("policy_byproductid"), itemDescriptionBeanId);
			itemDescriptionBeanId
					.setBack_url("ProductInfo.jsp?policy_byproductid=" + itemDescriptionBeanId.getParent_product_id());
			itemDescriptionBeanId.setType_page("policy_byproductid");
			itemDescriptionBeanId.setIntUserID(authorizationPageBeanId.getIntUserID());
			authorizationPageBeanId.setLastProductId(Long.parseLong(itemDescriptionBeanId.getParent_product_id()));

			if (request.getParameter("rate") != null && isNumber(request.getParameter("rate"))) {
				int rate = Integer.parseInt(request.getParameter("rate"));
				policyFaced.setRatring1(rate, itemDescriptionBeanId.getProduct_id());
			}

			itemDescriptionBeanId.setRating1_xml(policyFaced.getRatring1XML(itemDescriptionBeanId.getProduct_id()));
			itemDescriptionBeanId.setBalans("" + policyFaced.getBalans(authorizationPageBeanId.getIntUserID()));

			try {
				if (itemDescriptionBeanId.isFirstOpen()) {

					policyFaced.setPaymentInfoPage(itemDescriptionBeanId, authorizationPageBeanId,
							request.getRemoteAddr());
					itemDescriptionBeanId.setFirstOpen(false);
				} else
					policyFaced.incrementShowPageStatistics(itemDescriptionBeanId);
			} catch (Exception ex) {
				log.error("Billing is not working ", ex);
			}

		} else if (request.getParameter("page") != null && request.getParameter("page").compareTo("about") == 0) {
			policyFaced.setProductInfoBeanForAboutPage(authorizationPageBeanId.getSite_id(), itemDescriptionBeanId);// (authorizationPageBeanId.getIntUserID(),
			// request.getParameter("policy_byproductid")
			// ,
			// policyBeanId)
			// ;
			authorizationPageBeanId.setLastProductId(Long.parseLong(itemDescriptionBeanId.getParent_product_id()));
			itemDescriptionBeanId.setBack_url("ProductlistMarketPlace.jsp");
			itemDescriptionBeanId.setType_page("about");
			itemDescriptionBeanId.setIntUserID(authorizationPageBeanId.getIntUserID());
			itemDescriptionBeanId.setRating1_xml(policyFaced.getRatring1XML(itemDescriptionBeanId.getProduct_id()));
			itemDescriptionBeanId.setBalans("" + policyFaced.getBalans(authorizationPageBeanId.getIntUserID()));

			if (itemDescriptionBeanId.isFirstOpen()) {
				policyFaced.setPaymentInfoPage(itemDescriptionBeanId, authorizationPageBeanId,
						request.getRemoteAddr());
				itemDescriptionBeanId.setFirstOpen(false);
			} else
				policyFaced.incrementShowPageStatistics(itemDescriptionBeanId);
		} else if (request.getParameter("page") != null && request.getParameter("page").compareTo("pay") == 0) {
			policyFaced.setProductInfoBeanForPayPageInfo(authorizationPageBeanId.getSite_id(), itemDescriptionBeanId);// (authorizationPageBeanId.getIntUserID(),
			// request.getParameter("policy_byproductid")
			// ,
			// policyBeanId)
			// ;
			authorizationPageBeanId.setLastProductId(Long.parseLong(itemDescriptionBeanId.getParent_product_id()));
			itemDescriptionBeanId.setBack_url("ProductlistMarketPlace.jsp");
			itemDescriptionBeanId.setType_page("pay");
			itemDescriptionBeanId.setIntUserID(authorizationPageBeanId.getIntUserID());
			itemDescriptionBeanId.setRating1_xml(policyFaced.getRatring1XML(itemDescriptionBeanId.getProduct_id()));
			itemDescriptionBeanId.setBalans("" + policyFaced.getBalans(authorizationPageBeanId.getIntUserID()));

			if (itemDescriptionBeanId.isFirstOpen()) {
				policyFaced.setPaymentInfoPage(itemDescriptionBeanId, authorizationPageBeanId,
						request.getRemoteAddr());
				itemDescriptionBeanId.setFirstOpen(false);
			} else
				policyFaced.incrementShowPageStatistics(itemDescriptionBeanId);

		}

		itemDescriptionBeanId.setSelectCatalogXMLUrlPath(
				(new CatalogListBean(servletContext)).getCatalogXMLUrlPath("ProductlistMarketPlace.jsp?catalog_id",
						"parent", authorizationPageBeanId.getCatalog_id(), authorizationPageBeanId));
		itemDescriptionBeanId.setSelect_tree_catalog(productlistFaced.getTreeXMLDBList(
				"ProductlistMarketPlace.jsp?catalog_id", "catalog", authorizationPageBeanId.getCatalog_id(),
				"select catalog_id , lable   from catalog   where  active = true and site_id = "
						+ authorizationPageBeanId.getSite_id() + " and parent_id = "
						+ authorizationPageBeanId.getCatalogParent_id(),
				"select catalog_id , lable   from catalog   where  active = true and parent_id = "
						+ authorizationPageBeanId.getCatalog_id()));
		itemDescriptionBeanId.setSelect_currencies(
				policyFaced.getXMLDBList("ProductInfo.jsp", "currencies", itemDescriptionBeanId.getCurrency_cd(),
						"SELECT currency_cd , currency_desc  FROM currency  WHERE active = true"));
		itemDescriptionBeanId.columnOne = productlistFaced.getProductInfoColumnOne(
				authorizationPageBeanId.getIntUserID(), authorizationPageBeanId.getSite_id(),
				itemDescriptionBeanId.getParent_product_id(), itemDescriptionBeanId);
		itemDescriptionBeanId.columnTwo = productlistFaced.getProductInfoColumnTwo(
				authorizationPageBeanId.getIntUserID(), authorizationPageBeanId.getSite_id(),
				itemDescriptionBeanId.getParent_product_id(), itemDescriptionBeanId);
		itemDescriptionBeanId.attachedFiles = productlistFaced.getProductInfoAttchedFiles(
				authorizationPageBeanId.getIntUserID(), authorizationPageBeanId.getSite_id(),
				itemDescriptionBeanId.getParent_product_id(), itemDescriptionBeanId);
		itemDescriptionBeanId.descriptionTab = productlistFaced.getProductInfoDescriptionTabs(
				authorizationPageBeanId.getIntUserID(), authorizationPageBeanId.getSite_id(),
				itemDescriptionBeanId.getParent_product_id(), itemDescriptionBeanId);
		itemDescriptionBeanId.reviewMessages = productlistFaced.getProductInfoReviewMessages(
				authorizationPageBeanId.getIntUserID(), authorizationPageBeanId.getSite_id(),
				itemDescriptionBeanId.getParent_product_id(), itemDescriptionBeanId);
		// if( policyBeanId.getPortlettype_id() != Layout.PORTLET_TYPE_BOTTOM )
		itemDescriptionBeanId.newArrivalItems = productlistFaced.getNewArrivalItems(
				authorizationPageBeanId.getIntUserID(), authorizationPageBeanId.getSite_id(), authorizationPageBeanId);
		itemDescriptionBeanId.footerLinksList = productlistFaced.getFooterLinksList(
				authorizationPageBeanId.getIntUserID(), authorizationPageBeanId.getSite_id(), authorizationPageBeanId);

		itemDescriptionBeanId.setSelect_menu_catalog(productlistFaced.getMenuXMLDBList(
				"ProductlistMarketPlace.jsp?catalog_id", "menu", authorizationPageBeanId.getCatalog_id(),
				"select catalog_id , lable , parent_id  from catalog   where  active = true and parent_id = -2 and site_id = "
						+ authorizationPageBeanId.getSite_id() + " and lang_id = "
						+ authorizationPageBeanId.getLang_id()
						+ " or parent_id in (select catalog_id   from catalog   where  active = true and site_id = "
						+ authorizationPageBeanId.getSite_id() + "  and parent_id = -2 )"));

	}

	public boolean isNumber(String tmp) {
		if (tmp == null)
			return false;
		String IntField = "0123456789";
		for (int i = 0; i < tmp.length(); i++) {

			if (IntField.indexOf(tmp.charAt(i)) == -1) {
				if (tmp.charAt(i) != '-' && i != 0)
					return false;
			}
		}
		return true;
	}
}
