package com.cbsinc.cms.faceds;

/**
 * <p>
 * Title: Content Manager System
 * </p>
 * <p>
 * Description: System building web application develop by Konstantin Grabko.
 * Konstantin Grabko is Owner and author this code.
 * You can not use it and you cannot change without written permission from Konstantin Grabko
 * Email: konstantin.grabko@yahoo.com or konstantin.grabko@gmail.com
 * </p>
 * <p>
 * Copyright: Copyright (c) 2014
 * </p>
 * <p>
 * Company: CENTER BUSINESS SOLUTIONS INC 
 * </p>
 * 
 * @author Konstantin Grabko
 * @version 1.0
 */
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.cbsinc.cms.AuthorizationPageBean;
import com.cbsinc.cms.CurrencyHash;
import com.cbsinc.cms.NotificationsBean;
import com.cbsinc.cms.OrderListBean;
import com.cbsinc.cms.QueryManager;
import com.cbsinc.cms.controllers.SpecialCatalog;

public class NotificationsFaced extends com.cbsinc.cms.WebControls {

	/**
	 * <p>
	 * Title: Content Manager System
	 * </p>
	 * <p>
	 * Description: System building web application develop by Konstantin Grabko.
	 * Konstantin Grabko is Owner and author this code. You can not use it and you
	 * cannot change it without written permission from Konstantin Grabko Email:
	 * konstantin.grabko@yahoo.com or konstantin.grabko@gmail.com
	 * </p>
	 * <p>
	 * Copyright: Copyright (c) 2014
	 * </p>
	 * <p>
	 * Company: CENTER BUSINESS SOLUTIONS INC
	 * </p>
	 * 
	 * @author Konstantin Grabko
	 * @version 1.0
	 */

	static private Logger log = Logger.getLogger(OrderFaced.class);

	ResourceBundle sequences_rs = null;
	// transient java.util.Calendar calendar;
	// SimpleDateFormat formatter ;

	public NotificationsFaced() {
		if (sequences_rs == null)
			sequences_rs = PropertyResourceBundle.getBundle("sequence");
		// calendar = java.util.Calendar.getInstance();
		// formatter = new SimpleDateFormat(datePattern, locale );
	}


	final public String getNotificatios( final NotificationsBean notificationsBean ,  AuthorizationPageBean authorizationPageBean, Locale locale) {
		
		String product_description = "";
		String product_version = "";
		String product_cost = "";
		String currency_id = "";
		String currency_desc = "";
		String image_id = "";
		String product_fulldescription = "";
		String user1_id = "";
		String offer_status = "";
		String offer_status_id = "";
		String actionbidstatus = "";
		String actionbidstatus_id = "";
		String message = "" ;
		String file_exist1 = "";
		
		String product_id = "";
		String product_name = "";
		String product_url = "";
		String product_iconurl = "";
		String img_url = "";
		String fileId= "" ;
		
		String paystatus_id = "" ;
		String paystatus = "" ;
		
		String deliverystatus_id = "" ;
		String deliverystatus = "" ;
		
		String shipping_company = "" ;
		String tracking_number = "" ;
		
		String order_id = "" ;
		String cdate = "" ;

		
		notificationsBean.setListup("Notifications.jsp.jsp?offset=" + (notificationsBean.getOffset() + 10));
		if (notificationsBean.getOffset() - 10 < 0)
			notificationsBean.setListdown("Notifications.jsp.jsp?offset=0");
		else
			notificationsBean.setListdown("Notifications.jsp.jsp?offset=" + (notificationsBean.getOffset() - 10)); 
		 //LEFT JOIN shipping_tracking ON soft_hist.soft_id = shipping_tracking.soft_id
		StringBuffer table = new StringBuffer();
		QueryManager qm = new QueryManager();
		String query = "";
		query = "SELECT  soft_hist.soft_id, soft_hist.name,soft_hist.description, soft_hist.version, soft_hist.cost, soft_hist.currency, soft_hist.serial_nubmer, file.file_id, soft_hist.type_id, soft_hist.active , soft_hist.phonetype_id , soft_hist.progname_id  , soft_hist.image_id , images.img_url as img_url_s , soft_hist.fulldescription , big_images.img_url as img_url_b , soft_hist.user_id  , soft_hist.CDATE , offerstatus.LABLE , offerstatus.OFFER_STATUS_ID  "
				+ ",  soft_hist.amount1 ,  soft_hist.amount2 ,  soft_hist.amount3 ,   soft_hist.search2 ,  soft_hist.name2 ,  soft_hist.show_rating1 ,  soft_hist.show_rating2 ,  soft_hist.show_rating3 ,  soft_hist.show_blog ,  soft_hist.jsp_url , soft_hist.COLOR , subscription.message , actionbidstatus.LABLE , actionbidstatus.ACTION_BID_STATUS_ID  "
				+ " FROM soft_hist LEFT  JOIN subscription ON soft_hist.soft_id = subscription.soft_id  "
				+ " LEFT  JOIN images ON soft_hist.image_id = images.image_id  "
				+ " LEFT  JOIN big_images ON soft_hist.bigimage_id = big_images.big_images_id  "
				+ " LEFT  JOIN file  ON  soft_hist.file_id = file.file_id  "
				+ " LEFT  JOIN offerstatus  ON  soft_hist.OFFER_STATUS_ID = offerstatus.OFFER_STATUS_ID  "
				+ " LEFT  JOIN actionbidstatus  ON  soft_hist.ACTION_BID_STATUS_ID = actionbidstatus.ACTION_BID_STATUS_ID  "
				+ " WHERE  soft_hist.active = true and soft_hist.lang_id = ? " 
				+ " and subscription.user_id = ? " 
				+ " ORDER BY soft_hist.soft_id DESC "; // limit " + limit_news_list + " offset " +
		
		//+ "LEFT OUTER   JOIN paystatus  ON orders.paystatus_id = paystatus.paystatus_id  "
		//+ "LEFT OUTER   JOIN deliverystatus  ON orders.deliverystatus_id = deliverystatus.deliverystatus_id  "

		Object[] args = new Object[2];
		args[0] = Long.valueOf(authorizationPageBean.getLang_id());
		args[1] = Long.valueOf(authorizationPageBean.getIntUserID());

		try {
			qm.executeQueryWithArgs(query, args, 10, notificationsBean.getOffset());

			table.append("<notifications>\n");
			for (int i = 0; qm.rows().size() > i; i++) {
				
				
				product_description = (String) qm.getValueAt( i, 2);
				product_version = (String) qm.getValueAt(i, 3);
				product_cost = (String) qm.getValueAt( i, 4);
				currency_id = (String) qm.getValueAt( i, 5);
				CurrencyHash currencyHash = CurrencyHash.getInstance();
				currency_desc = currencyHash.getCurrency_decs(currency_id);
				image_id = (String) qm.getValueAt( i, 12);
				product_fulldescription = (String) qm.getValueAt( i, 14);
				user1_id = (String) qm.getValueAt( i, 16);
				cdate = (String) qm.getValueAt( i, 17);
				offer_status = (String) qm.getValueAt( i, 18);
				offer_status_id = (String) qm.getValueAt( i, 19);
				message = (String) qm.getValueAt( i, 31);
				product_id = (String) qm.getValueAt( i, 0);
				fileId =  (String) qm.getValueAt( i, 7);
				product_name = (String) qm.getValueAt( i, 1);
				product_url = "ProductInfo.jsp?policy_byproductid=" + (String) qm.getValueAt( i, 0);
				product_iconurl = (String) qm.getValueAt( i, 13);
				if (product_iconurl == null) product_iconurl = "images/Folder.jpg";
				img_url = (String) qm.getValueAt( i, 15);
				if (img_url == null) img_url = "images/Folder.jpg";
				if ( fileId != null && fileId.length() > 0) file_exist1 = "true";
				actionbidstatus = (String) qm.getValueAt( i, 32);
				actionbidstatus_id = (String) qm.getValueAt( i, 33);
				 

				table.append("<notification>\n");
				table.append("<product_id>" + product_id + "</product_id>\n");
				table.append("<message>" + message + "</message>\n");
				table.append("<row_id>" + i + "</row_id>\n");
				table.append("<file_exist>" + file_exist1 + "</file_exist>\n");
				table.append("<name>" + product_name + "</name>\n");
				table.append("<image>" + product_iconurl + "</image>\n");
				table.append("<big_image>" + img_url + "</big_image>\n");
				table.append("<big_image_type>" + img_url.substring(img_url.indexOf(".") + 1) + "</big_image_type>\n");
				table.append("<icon_type>" + product_iconurl.substring(product_iconurl.indexOf(".") + 1) + "</icon_type>\n");
				table.append("<user_id>" + user1_id + "</user_id>\n");
				table.append("<product_url>" + product_url + "</product_url>\n");
				table.append("<description>" + product_description + "</description>\n");
				table.append("<amount>" + product_cost + "</amount>\n");
				table.append("<offer_status>" + offer_status + "</offer_status>\n");
				table.append("<offer_status_id>" + offer_status_id + "</offer_status_id>\n");
				table.append("<actionbidstatus>" + actionbidstatus + "</actionbidstatus>\n");
				table.append("<actionbidstatus_id>" + actionbidstatus_id + "</actionbidstatus_id>\n");
				
				table.append("<paystatus>" + paystatus + "</paystatus>\n");
				table.append("<paystatus_id>" + paystatus_id + "</paystatus_id>\n");

				table.append("<deliverystatus>" + deliverystatus + "</deliverystatus>\n");
				table.append("<deliverystatus_id>" + deliverystatus_id + "</deliverystatus_id>\n");
				table.append("<cdate>" + cdate + "</cdate>\n");
				
				table.append("<currency_desc>" + currency_desc + "</currency_desc>\n");
				table.append("</notification>\n");
			}

			

			
			table.append("</notifications>\n");
		} catch (SQLException ex) {

			log.error(query, ex);
		} catch (Exception ex) {

			log.error(ex);
		} finally {
			qm.close();
		}
		return table.toString();

	}

	final public String getNotificationByDate(final long user_id, final NotificationsBean orderListBean, final Locale locale,
			long role_id, String site_id) {

		String orderId = "";
		String productName = "0";
		String productCost = "0";
		String cdate = "";
		String paystatusLable = "";
		String paystatusId = "";
		String itemDeliverystatusLable = "";
		String itemDeliverystatusId = "";
		String currencyLable = "" ;
		String shippingCompanyName = "" ;
		String trackingNumber = "" ;
		
		orderListBean.setListup("Notifications.jsp?offset=" + (orderListBean.getOffset() + 10));
		if (orderListBean.getOffset() - 10 < 0)
			orderListBean.setListdown("Notifications.jsp?offset=0");
		else
			orderListBean.setListdown("Notifications.jsp?offset=" + (orderListBean.getOffset() - 10));

		StringBuffer table = new StringBuffer();
		QueryManager Adp = new QueryManager();
		String query = "";
		Object[] args = null;
		if (role_id == 2) {
			query = "SELECT orders_hist.ORDER_ID, orders_hist.NAME , orders_hist.PRODUCT_COST, orders_hist.cdate ,  itemdeliverystatus.lable ,  paystatus.lable ,  itemdeliverystatus.ITEM_DELIVERYSTATUS_ID , "
					+ " paystatus.paystatus_id  , currency.currency_lable , shipping_company.NAME , shipping_tracking.SHIPPING_NUNBER  FROM orders_hist "
					+ " LEFT  JOIN shipping_company ON orders_hist.SHIPPING_COMPANY_ID  =  shipping_company.SHIPPING_COMPANY_ID "
					+ " LEFT  JOIN shipping_tracking ON orders_hist.soft_id  =  shipping_tracking.soft_id "
					+ " LEFT  JOIN paystatus ON orders_hist.paystatus_id  =  paystatus.paystatus_id "
					+ " LEFT  JOIN tuser ON orders_hist.user_id = tuser.user_id "
					+ " LEFT  JOIN itemdeliverystatus ON orders_hist.ITEM_DELIVERYSTATUS_ID  =  itemdeliverystatus.ITEM_DELIVERYSTATUS_ID "
					+ " LEFT  OUTER JOIN currency ON orders_hist.CURRENCY = currency.currency_id "
					+  "WHERE tuser.site_id  = ? and orders_hist.cdate >= ? and orders_hist.cdate <=  ? "; // LIMIT 10 OFFSET ? " ;


			args = new Object[3];
			args[0] = Long.valueOf(site_id);
			args[1] = orderListBean.getSQLDateFrom();
			args[2] = orderListBean.getSQLDateTo();

		} else {
			query = "SELECT orders_hist.ORDER_ID, orders_hist.NAME , orders_hist.PRODUCT_COST, orders_hist.cdate ,  itemdeliverystatus.lable ,  paystatus.lable ,  itemdeliverystatus.ITEM_DELIVERYSTATUS_ID , "
					+ " paystatus.paystatus_id  , currency.currency_lable , shipping_company.NAME , shipping_tracking.SHIPPING_NUNBER  FROM orders_hist "
					+ " LEFT  JOIN shipping_company ON orders_hist.SHIPPING_COMPANY_ID  =  shipping_company.SHIPPING_COMPANY_ID "
					+ " LEFT  JOIN shipping_tracking ON orders_hist.soft_id  =  shipping_tracking.soft_id "
					+ " LEFT  JOIN paystatus ON orders_hist.paystatus_id  =  paystatus.paystatus_id "
					+ " LEFT  JOIN tuser ON orders_hist.user_id = tuser.user_id "
					+ " LEFT  JOIN itemdeliverystatus ON orders_hist.ITEM_DELIVERYSTATUS_ID  =  itemdeliverystatus.ITEM_DELIVERYSTATUS_ID "
					+ " LEFT  OUTER JOIN currency ON orders_hist.CURRENCY = currency.currency_id "
					+  "WHERE orders_hist.user_id  = ? and orders_hist.cdate >= ? and orders_hist.cdate <=  ? "; // LIMIT 10 OFFSET ? " ;
			

			args = new Object[3];
			args[0] = Long.valueOf(user_id);
			args[1] = orderListBean.getSQLDateFrom();
			args[2] = orderListBean.getSQLDateTo();

		}



		try {
			Adp.executeQueryWithArgs(query, args, 10, orderListBean.getOffset());
			// Adp.executeQuery(query);

			table.append("<list>\n");
			for (int i = 0; Adp.rows().size() > i; i++) {
				orderId = (String) Adp.getValueAt(i, 0);
				productName = (String) Adp.getValueAt(i, 1);
				productCost = (String) Adp.getValueAt(i, 2);
				cdate = (String) Adp.getValueAt(i, 3);
				itemDeliverystatusLable = (String) Adp.getValueAt(i, 4);
				paystatusLable = (String) Adp.getValueAt(i, 5);
				itemDeliverystatusId = (String) Adp.getValueAt(i, 6);
				paystatusId = (String) Adp.getValueAt(i, 7);
				currencyLable = (String) Adp.getValueAt(i, 8);
				shippingCompanyName = (String) Adp.getValueAt(i, 9);
				trackingNumber = (String) Adp.getValueAt(i, 10);
				//orderListBean.setCurrency_lable((String) Adp.getValueAt(i, 5));
				table.append("<purchase>\n");
				table.append("<orderId>" + orderId + "</orderId>\n");
				table.append("<productName>" + productName + "</productName>\n");
				table.append("<productCost>" + getStrFormatNumberFloat(productCost) + "</productCost>\n");
				try {
					table.append("<cdate>"+ orderListBean.getSimpleDateFormat(locale).format(Adp.getSimpleDateFormat().parse(cdate)) + "</cdate>\n");
				} catch (ParseException ex) {
					log.error(ex);
				}
				table.append("<itemDeliverystatusLable>" + itemDeliverystatusLable + "</itemDeliverystatusLable>\n");
				table.append("<itemDeliverystatusId>" + itemDeliverystatusId + "</itemDeliverystatusId>\n");
				table.append("<paystatus_lable>" + paystatusLable + "</paystatus_lable>\n");
				table.append("<paystatusId>" + paystatusId + "</paystatusId>\n");
				table.append("<currency_lable>" +currencyLable + "</currency_lable>\n");
				table.append("<shippingCompanyName>" +shippingCompanyName + "</shippingCompanyName>\n");
				table.append("<trackingNumber>" +trackingNumber + "</trackingNumber>\n");
				table.append("</purchase>\n");
			}

			table.append("</list>\n");
			
		} catch (SQLException ex) {

			log.error(query, ex);
		} catch (Exception ex) {

			log.error(ex);
		} finally {
			Adp.close();
		}
		return table.toString();

	}

	final public String getNotificationByStatus(final String site_id, final NotificationsBean orderListBean,
			final Locale locale) {

		String orderId = "";
		String productName = "0";
		String productCost = "0";
		String cdate = "";
		String paystatusLable = "";
		String paystatusId = "";
		String itemDeliverystatusLable = "";
		String itemDeliverystatusId = "";
		String currencyLable = "" ;
		String shippingCompanyName = "" ;
		String trackingNumber = "" ;
		
		orderListBean.setListup("Notifications.jsp?offset=" + (orderListBean.getOffset() + 10));
		if (orderListBean.getOffset() - 10 < 0)
			orderListBean.setListdown("Notifications.jsp?offset=0");
		else
			orderListBean.setListdown("Notifications.jsp?offset=" + (orderListBean.getOffset() - 10));

		StringBuffer table = new StringBuffer();
		QueryManager Adp = new QueryManager();
		String query = "";
		query = "SELECT orders_hist.ORDER_ID, orders_hist.NAME , orders_hist.PRODUCT_COST, orders_hist.cdate ,  itemdeliverystatus.lable ,  paystatus.lable ,  itemdeliverystatus.ITEM_DELIVERYSTATUS_ID , "
				+ " paystatus.paystatus_id  , currency.currency_lable , shipping_company.NAME , shipping_tracking.SHIPPING_NUNBER  FROM orders_hist "
				+ " LEFT  JOIN shipping_company ON orders_hist.SHIPPING_COMPANY_ID  =  shipping_company.SHIPPING_COMPANY_ID "
				+ " LEFT  JOIN shipping_tracking ON orders_hist.soft_id  =  shipping_tracking.soft_id "
				+ " LEFT  JOIN paystatus ON orders_hist.paystatus_id  =  paystatus.paystatus_id "
				+ " LEFT  JOIN tuser ON orders_hist.user_id = tuser.user_id "
				+ " LEFT  JOIN itemdeliverystatus ON orders_hist.ITEM_DELIVERYSTATUS_ID  =  itemdeliverystatus.ITEM_DELIVERYSTATUS_ID "
				+ " LEFT  OUTER JOIN currency ON orders_hist.CURRENCY = currency.currency_id "
				+ "WHERE tuser.site_id  = ? and ( orders.paystatus_id = ? or orders.ITEM_DELIVERYSTATUS_ID =  ? )"; // LIMIT
	
		Object[] args = new Object[3];
		args[0] = Long.valueOf(site_id);
		args[1] = orderListBean.getOrder_paystatus_id();
		args[2] = orderListBean.getDeliverystatus_id();
		// args[3] = orderListBean.getOffset();

		try {
			Adp.executeQueryWithArgs(query, args, 10, orderListBean.getOffset());

			table.append("<list>\n");
			for (int i = 0; Adp.rows().size() > i; i++) {
				orderId = (String) Adp.getValueAt(i, 0);
				productName = (String) Adp.getValueAt(i, 1);
				productCost = (String) Adp.getValueAt(i, 2);
				cdate = (String) Adp.getValueAt(i, 3);
				itemDeliverystatusLable = (String) Adp.getValueAt(i, 4);
				paystatusLable = (String) Adp.getValueAt(i, 5);
				itemDeliverystatusId = (String) Adp.getValueAt(i, 6);
				paystatusId = (String) Adp.getValueAt(i, 7);
				currencyLable = (String) Adp.getValueAt(i, 8);
				shippingCompanyName = (String) Adp.getValueAt(i, 9);
				trackingNumber = (String) Adp.getValueAt(i, 10);
				//orderListBean.setCurrency_lable((String) Adp.getValueAt(i, 5));
				table.append("<purchase>\n");
				table.append("<orderId>" + orderId + "</orderId>\n");
				table.append("<productName>" + productName + "</productName>\n");
				table.append("<productCost>" + getStrFormatNumberFloat(productCost) + "</productCost>\n");
				try {
					table.append("<cdate>"+ orderListBean.getSimpleDateFormat(locale).format(Adp.getSimpleDateFormat().parse(cdate)) + "</cdate>\n");
				} catch (ParseException ex) {
					log.error(ex);
				}
				table.append("<itemDeliverystatusLable>" + itemDeliverystatusLable + "</itemDeliverystatusLable>\n");
				table.append("<itemDeliverystatusId>" + itemDeliverystatusId + "</itemDeliverystatusId>\n");
				table.append("<paystatus_lable>" + paystatusLable + "</paystatus_lable>\n");
				table.append("<paystatusId>" + paystatusId + "</paystatusId>\n");
				table.append("<currency_lable>" +currencyLable + "</currency_lable>\n");
				table.append("<shippingCompanyName>" +shippingCompanyName + "</shippingCompanyName>\n");
				table.append("<trackingNumber>" +trackingNumber + "</trackingNumber>\n");
				table.append("</purchase>\n");
			}

			table.append("</list>\n");
			
		} catch (SQLException ex) {

			log.error(query, ex);
		} catch (Exception ex) {

			log.error(ex);
		} finally {
			Adp.close();
		}
		return table.toString();

	}

}
