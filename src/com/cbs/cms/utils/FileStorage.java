package com.cbs.cms.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import javax.activation.MimetypesFileTypeMap;

import org.apache.log4j.Logger;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class FileStorage {

	private static Logger log = Logger.getLogger(FileStorage.class);
	private static FileStorage fileStorage = new FileStorage();
	//private  String path = System.getProperty("java.io.tmpdir");
	private  String path = "" ;

	private ResourceBundle setup_resources = null;

	private FileStorage() {

		if (setup_resources == null)
			setup_resources = PropertyResourceBundle.getBundle("appconfig");
		
		path = createPath() ;
		
		try {
		
			String file_dir_docker = System.getenv("FILE_DIR");
			if (file_dir_docker != null && !file_dir_docker.isEmpty()) {
				
				if(isExistFileDir(file_dir_docker) && isDirectoryEmpty( file_dir_docker ) ) 
				{
					copyFiles(path, file_dir_docker) ;
				}
				log.info("The file_dir variable got from docker enviroment variables. -Djava.io.tmpdir=C:\\temp to change that ");
				
				path = 	file_dir_docker ;
				
			} else {
				
				String file_dir = setup_resources.containsKey("file_dir")  ? setup_resources.getString("file_dir"):null;
				if (file_dir != null && !file_dir.isEmpty()) {
					if(isExistFileDir(file_dir) && isDirectoryEmpty( file_dir ) ) 
					{
						copyFiles(path, file_dir) ;
					}
					path = 	file_dir ;
					log.info("The file_dir variable was overwritten from appconfig.properties file ");
				} else {
					log.info("The file_dir variable got from system enviroment variables. -Djava.io.tmpdir=C:\\temp to change that ");
				}
			}
		
		}catch (Throwable e) {
			log.error(e);
		}

		log.info("file_dir:" + path);

	}
	
	private String createPath() 
	{
		String path = this.getClass().getResource("").getPath();
		if(OSUtil.isWindows())  path = path.substring(1, path.indexOf("/WEB-INF/"));
		else path = path.substring(0, path.indexOf("/WEB-INF/"));
		if(path.contains("%20")) path = path.replaceAll("%20", " ") ;
		return path;
	}

	public static FileStorage getInstance() {
		return fileStorage;

	}

	public String getPath() {
		return path;
	}
	
	public boolean isExistFileDir(String directoryPath )
	{
		boolean result = false ;
		File directory = new File(directoryPath);
		if (!directory.exists()) {
            if (directory.mkdirs()) {
            	result = true ;
            	log.info("File_dir directory created successfully!");
            } else {
            	result = false ;
            	log.info("File_dir Failed to create directory.");
            }
        } else {
        	result = true ;
        	log.info("File_dir directory already exists.");
        }
		
		return result ;
	}

	
	void copyFiles(String sourceDirectory   , String targetDirectory  ) 
	{
		Path sourceDir = Paths.get(sourceDirectory);
        Path targetDir = Paths.get(targetDirectory);

        try {
            Files.walkFileTree(sourceDir, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    Path targetPath = targetDir.resolve(sourceDir.relativize(dir));
                    if (!Files.exists(targetPath)) {
                        Files.createDirectory(targetPath);
                    }
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.copy(file, targetDir.resolve(sourceDir.relativize(file)), StandardCopyOption.REPLACE_EXISTING);
                    return FileVisitResult.CONTINUE;
                }
            });

            log.info("Files copied to "+ targetDirectory +" successfully!");
        } catch (Throwable e) {
        	log.error(e);
        }

	}
	
	boolean isDirectoryEmpty(String directoryPath )
	{
		boolean result = false ;
		Path path = Paths.get(directoryPath);
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path)) {
            if (!directoryStream.iterator().hasNext()) {
            	result = true ;
            } else {
            	result = false ;
            }
        } catch (Throwable e) {
        	result = false ;
        	log.error(e);
        }
		return result;
	}
	
	
	public String validateFilesPath(String urlAfterWebDomain) throws IOException{
		 String validPath = urlAfterWebDomain ;
		try {
			String path = FileStorage.getInstance().getPath();
	         String relativeImagePath = urlAfterWebDomain.substring(urlAfterWebDomain.indexOf("/files/"));  
	         validPath = path+relativeImagePath ;
	        log.debug("\nFetching files from "+validPath);
	        return validPath ;

		}catch (Throwable e) {
			validPath = urlAfterWebDomain ;
			log.error(e);
		}
		return validPath;
    }
	
	public String validateImgpositionsPath(String urlAfterWebDomain) throws IOException{
		 String validPath = urlAfterWebDomain ;
		try {
			String path = FileStorage.getInstance().getPath();
	         String relativeImagePath = urlAfterWebDomain.substring(urlAfterWebDomain.indexOf("/imgpositions/"));  
	         validPath = path+relativeImagePath ;
	        log.debug("\nFetching imgpositions from "+validPath);
	        return validPath ;

		}catch (Throwable e) {
			validPath = urlAfterWebDomain ;
			log.error(e);
		}
		return validPath;
   }
	
	public String validateBigImgpositionsPath(String urlAfterWebDomain) throws IOException{
		 String validPath = urlAfterWebDomain ;
		try {
			String path = FileStorage.getInstance().getPath();
	         String relativeImagePath = urlAfterWebDomain.substring(urlAfterWebDomain.indexOf("/big_imgpositions/"));  
	         validPath = path+relativeImagePath ;
	        log.debug("\nFetching big_imgpositions from "+validPath);
	        return validPath ;

		}catch (Throwable e) {
			validPath = urlAfterWebDomain ;
			log.error(e);
		}
		return validPath;
  }
	
	public String validateImgcatalogPath(String urlAfterWebDomain) throws IOException{
		 String validPath = urlAfterWebDomain ;
		try {
			String path = FileStorage.getInstance().getPath();
	         String relativeImagePath = urlAfterWebDomain.substring(urlAfterWebDomain.indexOf("/imgcatalog/"));  
	         validPath = path+relativeImagePath ;
	        log.debug("\nFetching imgcatalog from "+validPath);
	        return validPath ;

		}catch (Throwable e) {
			validPath = urlAfterWebDomain ;
			log.error(e);
		}
		return validPath;
 }	
	
}
