<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<%@ page errorPage="error.jsp" %>
<%
  response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
  response.setHeader("Pragma","no-cache"); //HTTP 1.0
  response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
  request.setCharacterEncoding("UTF-8");
%>
<html>
<HEAD>
<jsp:useBean id="publisherBeanId" scope="session" class="com.cbsinc.cms.PublisherBean" />
<jsp:useBean id="authorizationPageBeanId" scope="session" class="com.cbsinc.cms.AuthorizationPageBean" />
<jsp:setProperty name="publisherBeanId" property="*" />
<title><%=authorizationPageBeanId.getLocalization(application).getString("title_select_big_image")%></title>
<script language="JavaScript">
        <!--
        function setData(){
        	parent.catalog_edit.catalogImagename.value = '<%= publisherBeanId.getCatalogImgname() %>'  ;
        	parent.catalog_edit.catalogImageId.value =  '<%= publisherBeanId.getCatalogImage_id() %>'  ;
        	parent.dwindow('SelectCatalogImage.jsp'); 
        return true ;
        }

		function setEmpty(){
        top.catalog_edit.catalogImagename.value = ''  ;
        top.catalog_edit.catalogImageId.value =  -1  ;
        top.dwindow('SelectCatalogImage.jsp'); 
        return true ;
        }

        function setClose(){
        	parent.dwindow('SelectCatalogImage.jsp'); 
        return true ;
        }
        
        
        function changeImage(){
		document.forms["selectCatalogImage"].submit();
        return true ;
		}
		
        //-->
</script>
</HEAD><BODY>
<form method="post" name="selectCatalogImage"   ACTION="SelectCatalogImage.jsp"  >
<TABLE>
<TR><TD colspan="3" ><%=authorizationPageBeanId.getLocalization(application).getString("title_select_category_image")%></TD></TR>
<TR><TD colspan="3" ><%=publisherBeanId.getSelect_catalog_images()%></TD></TR>
<TR><TD><input type="submit" name="Submit" value="<%= authorizationPageBeanId.getLocalization(application).getString("apply") %>"  onclick="return setData()"  ></TD><TD><input type="button" value="<%= authorizationPageBeanId.getLocalization(application).getString("select_with_out_pic") %>" onClick="return setEmpty()" ></TD></TR>
</TABLE>
</form>
<img  id="bigimage"  height="160" alt="Current image" src="<%= publisherBeanId.getSelect_catalog_image_url() %>"  >
</body>
</html>
