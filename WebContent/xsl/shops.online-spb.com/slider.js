
function rightScroll() {
  document.querySelector('.scroll-content').scrollBy({
    left: 200,
    behavior: 'smooth'
  });
}


function leftScroll() {
  document.querySelector('.scroll-content').scrollBy({
    left: -200,
    behavior: 'smooth'
  });
}


function rightScrollNewArrival() {
  document.querySelector('.scroll-content-new-arrival').scrollBy({
    left: 200,
    behavior: 'smooth'
  });
}


function leftScrollNewArrival() {
  document.querySelector('.scroll-content-new-arrival').scrollBy({
    left: -200,
    behavior: 'smooth'
  });
}



function rightScrollRecommendedItems() {
  document.querySelector('.scroll-content-recommended-items').scrollBy({
    left: 200,
    behavior: 'smooth'
  });
}


function leftScrollRecommendedItems() {
  document.querySelector('.scroll-content-recommended-items').scrollBy({
    left: -200,
    behavior: 'smooth'
  });
}


function rightScrollSponsoredBySeller() {
  document.querySelector('.scroll-content-sponsored-by-seller').scrollBy({
    left: 200,
    behavior: 'smooth'
  });
}


function leftScrollSponsoredBySeller() {
  document.querySelector('.scroll-content-sponsored-by-seller').scrollBy({
    left: -200,
    behavior: 'smooth'
  });
}


