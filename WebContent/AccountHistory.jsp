<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<%@ page errorPage="error.jsp" %>
<jsp:useBean id="orderListBeanId" scope="session" class="com.cbsinc.cms.OrderListBean" />
<jsp:useBean id="accountHistoryBeanId" scope="session" class="com.cbsinc.cms.AccountHistoryBean" />
<jsp:useBean id="authorizationPageBeanId" scope="session" class="com.cbsinc.cms.AuthorizationPageBean" />
<jsp:setProperty name="accountHistoryBeanId" property="*" />
<%@page import="java.util.PropertyResourceBundle,java.util.ResourceBundle,java.io.*"%>

<%
response.setCharacterEncoding("UTF-8");
response.setContentType("text/xml");
String url = "" ;
String xsltpath = "";
String xsltpath_default = "" ;
String xsltUrl = "" ;
String xsltUrl_default = "" ;

try
{
String mobile = authorizationPageBeanId.isMobileSession()?"/mobile":""  ;
xsltUrl =  request.getScheme() + "://" + request.getServerName() +  ":"+request.getServerPort() + request.getContextPath() + "/xsl/" +  authorizationPageBeanId.getSite_dir()  + mobile + "/"  +  authorizationPageBeanId.getLocale() + "/" + "accounthistory.xsl" ; 
xsltUrl_default = request.getScheme() + "://" + request.getServerName() +  ":"+request.getServerPort() + request.getContextPath() + "/xsl/" +  authorizationPageBeanId.getSite_dir()  + mobile + "/"  + "accounthistory.xsl" ; 
xsltpath =  "xsl/" +  authorizationPageBeanId.getSite_dir() + mobile + "/"  +  authorizationPageBeanId.getLocale() + "/" + "accounthistory.xsl" ; 
xsltpath_default = "xsl/" +  authorizationPageBeanId.getSite_dir() + mobile + "/" + "accounthistory.xsl" ; 
xsltpath = request.getServletContext().getRealPath("/" +xsltpath);
xsltpath_default = request.getServletContext().getRealPath("/" +xsltpath_default);

	File file = new File(xsltpath) ;
	if( file == null  || !file.exists() ) url = xsltUrl_default ;
	else url = xsltUrl ;
		 
}
 catch (Exception e) 
{
	 throw e ;
}
finally {
	System.out.println("isMobileSession: " + authorizationPageBeanId.isMobileSession());
    System.out.println("AccountHistory.jsp xsltpath: " + xsltpath);
    System.out.println("AccountHistory.jsp xslt url: " + url);
}


PrintWriter printWriter = response.getWriter();
String tmp ="<?xml-stylesheet type=\"text/xsl\" href=\""+url+"\"?>" ;
printWriter.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
printWriter.println(tmp);

%>

<document>
   <version>1.0</version>
   <name>GBS ltd.</name>

   <virtual_host><%= authorizationPageBeanId.getVirtualHost() %></virtual_host>
   <title><%=  authorizationPageBeanId.getHost() %></title>
   <subject_site><%=  authorizationPageBeanId.getSubject_site() %></subject_site>
   <site_name><%=  authorizationPageBeanId.getNick_site() %></site_name>
   <host><%=  authorizationPageBeanId.getSite_dir() %></host>
   <domain><%=  authorizationPageBeanId.getHost() %></domain>
   <login><%= authorizationPageBeanId.getStrLogin() %></login>
   <passwdord></passwdord>
   <shoping_url>Productlist.jsp</shoping_url>
   <message><%= authorizationPageBeanId.getStrMessage() %></message>
   <shoping_url>Productlist.jsp</shoping_url>
   <balans><%=  accountHistoryBeanId.getStrBalans(authorizationPageBeanId.getIntUserID()) %></balans>
   <to_navigator>wCatalog.jsp</to_navigator>
   <to_navigator_location>NavigatorLocation.jsp</to_navigator_location>
   <to_account_history>AccountHistory.jsp</to_account_history>
   <to_login>Authorization.jsp</to_login>
   <to_registration>RegPage.jsp</to_registration>
   <to_order>Order.jsp</to_order>
   <to_order_hist>OrderList.jsp</to_order_hist>
   <to_pay>PrePay.jsp</to_pay>
   <datefrom>01/01/2025</datefrom>
   <dateto>01/01/2025</dateto>
   <%=accountHistoryBeanId.getSelectAccountHistoryXML()  %>
   
    <%=  orderListBeanId.getSelect_menu_catalog()	 %>

<next><jsp:getProperty name="accountHistoryBeanId" property="listup" /></next>
<prev><jsp:getProperty name="accountHistoryBeanId" property="listdown" /></prev>

</document>