# For Java 11, try this

FROM tomcat:10.1.30

#COPY target/cms-manhattan-one-0.0.5.war /usr/local/tomcat/webapps/ROOT.war

#COPY target/cms-manhattan-one-0.0.12.war /usr/local/tomcat/webapps/cms.war
COPY target/cms-manhattan-one-0.0.35.war /usr/local/tomcat/webapps/ROOT.war

##COPY target/cms-manhattan-one-0.0.8.war /opt/apache-tomcat-7.0.90/webapps/ROOT.war
COPY  tomcat/*  /usr/local/tomcat/conf/

