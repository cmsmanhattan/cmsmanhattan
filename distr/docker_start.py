import subprocess
import sys

def docker_start():
    try:
        subprocess.run(["docker", "compose", "up", "--build"], check=True)
    except subprocess.CalledProcessError as e:
        print(f"Error starting Docker: {e}")
        sys.exit(1)

if __name__ == "__main__":
    docker_start()
