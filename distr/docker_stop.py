import subprocess
import sys

def docker_stop():
    try:
        subprocess.run(["docker", "compose", "down", "-v"], check=True)
    except subprocess.CalledProcessError as e:
        print(f"Error stopping Docker: {e}")
        sys.exit(1)

if __name__ == "__main__":
    docker_stop()